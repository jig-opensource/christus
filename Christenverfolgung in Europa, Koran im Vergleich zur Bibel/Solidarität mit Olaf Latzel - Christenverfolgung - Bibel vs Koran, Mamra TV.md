Abdul vom YouTube-Kanal Memra TV hat einen sehr guten Beitrag zu den Ideologien unserer Zeit erstellt.

**Sein Thema:**

Link zu YouTube: [Solidarität mit Olaf Latzel - Wacht auf, Christen! - Christenverfolgung in Deutschland](https://www.youtube.com/watch?v=tc6E3dt3Q1k)

**Sein Inhalt:**
- In einer Predigt hat Pastor Olaf Latzel gezeigt, 
  - dass der Islam und das Christentum ganz offensichtlich nicht den gleichen Gott haben können
  - und dass Christen der biblisch geforderte Nächstenliebe folgen müssen und dürfen.
  - In der Praxis heisst dies: wir müssen uns vor Muslime stellen, wenn sie verfolgt werden.
- Der Staat wie auch die Medien zeigen mit der Anklage wegen Volksverhetzung eine furchtbar ideologische Doppelmoral und Heuchlerei
- Abdul vergleicht die Aussagen der Bibel mit dem Koran - sehr sachlich und fair


**Inhalt im YouTube-Video**

- [0000 - Start: Christenverfolgung in Deutschland](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=8)
- [0010 - Olaf Latzel - Anklage wegen Volksverhetzung weil er sagte, dass Christen und Muslime nicht den selben Gott haben](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=38)
  - [0020 - Wird uns da nicht eien Doppelmoral und Heuchlerei vor Augen gemalt](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=98)
  - [0030 - Ignoranz gegenüber der salafistischen Prediger](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=105)
  - [0040 - Die Meinungsfreiheit ist selektiv](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=134)
- [0100 - Zur Kritik an der Predigt von Olaf Latzel](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=214)
- [0200 - Wir können nicht alle denselben Gott haben](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=434)
  - [0210- Es kann ja nur einen Schöpfer geben, aber sehr viele unterschiedliche Gottes-Bilder](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=473)
- [0300 - Plakat mit den widersprüchlichen eigenschaften Gottes - Bibel vs Koran](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=503)
  - [0310 - Plakat - Jesus ist Gott, der Schöpfer](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=573)
  - [0320 - Plakat - Die Kreuzigung Jesu - Wir haben ausserbiblische Quellen](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=693)
  - [0330 - Plakat - Inspiration der Bibel](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=816)
  - [0340 - Plakat - Der Absolutheitsanspruch beider Religionen](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=905)
  - [0350 - Plakat - Die Sohnschaft Jesus](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1014)
  - [0360 - Wie können wir da gemeinsam beten](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1141)
- [0400 - Olaf Latzel - Seine Aussage - Wir müssen allen Muslimen in Liebe Begegnen!](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1169)
  - [0410 - Zeige einen muslimischen Prediger oder eine Stelle im Koran, in der die Liebe zu Juden, Christen oder Atheisten gefordert wird](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1180)
  - [0420 - Olaf Latzel müsste einen Friedensnobelpreis erhalten](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1190)
  - [0430 - Bitte mehr von solchen Pastoren - weniger Allversöhnung](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1199)
- [0500 - Gott will, dass alle Menschen gerettet und zur Erkenntnis der Wahrheit kommen](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1229)
  - [0510 - Die wunderbare Botschaft - Wir dürfen nicht nur die lieben, die uns lieben!](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1250)
  - [0520 - Liebe und Barmherzigkeit den Muslimen gegenüber - wo hört man solche Worte von Muslimen gegenüber Andersgläubigen](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1293)
  - [0530 - Wir haben Muslime zu lieben und wenn sie verfolgt werden, müssen wir uns vor sie stellen](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1363)
- [0600 - Der Islam verfolgt die Christen](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1391)
  - [0610 - Die ISIS schlachtet und versklavt Menschen](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1411)
- [0700 - Jesu Liebe kostete ihn sein Leben](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1430)
  - [0710 - DE - Das Gleichbehandlungsgesetzt](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1450)
- [0800 - Der Koran - wie sollen wir mit Ungläubigen umgehen - Sie sind schlimmer als Schweine](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1530)
  - [0810 - Was ist Volksverhetzung - warum wird islamische Volksverhetzung nicht vorgegangen](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1556)
  - [0820 - Sure 5,51 Nehmt euch keine Christen und Juden nicht zu Freunden](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1617)
  - [0830 - Der Koran widerpricht derm Grundgesetz vielfältig - dieses Bild kennt die Bibel nicht](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1661)
- [0900 - Ein Wort an 'christliche' Heuchler](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1690)
  - [0910 - Die Bibel lehrt und zwei Arten von Christen - Nachfolger und liebende - und Heuchler](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1720)
  - [0920 - 1 Joh 2 - Sie sind von uns ausgegangen, aber waren nicht von uns](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1750)
- [1000 - Tröstendes Wort an Olaf Latzel](https://www.youtube.com/watch?v=tc6E3dt3Q1k&feature=youtu.be&t=1820)


